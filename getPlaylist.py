# -*- coding: utf-8 -*-
# By Ricardo Arreola, youtube Api doc and stackOverflow

import os
import re
import math
import json
from os import walk
import urllib.request
import googleapiclient.errors
import googleapiclient.discovery
import google_auth_oauthlib.flow
from PIL import Image, ImageEnhance


class yoto:
    scopes = ["https://www.googleapis.com/auth/youtube",
              "https://www.googleapis.com/auth/youtube.force-ssl",
              "https://www.googleapis.com/auth/youtube.readonly",
              "https://www.googleapis.com/auth/youtubepartner"]

    def __init__(self, playlist_id):
        self.playlist = playlist_id
        self.response = {}
        self.PIC_FOLDER = playlist_id + '-pictures/'
        if not os.path.exists(self.PIC_FOLDER):
            os.mkdir(self.PIC_FOLDER)
        self.flow = None

    def request(self, pageToken=None):
        # Disable OAuthlib's HTTPS verification when running locally.
        # *DO NOT* leave this option enabled in production.
        os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

        api_service_name = "youtube"
        api_version = "v3"
        client_secrets_file = "client_secret.json"

        # Get credentials and create an API client
        if self.flow is None:
            self.flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
                client_secrets_file, self.scopes)
            self.credentials = self.flow.run_console()
            self.youtube = googleapiclient.discovery.build(
                api_service_name, api_version, credentials=self.credentials)

        if pageToken:
            drequest = self.youtube.playlistItems().list(
                part="snippet",
                maxResults=100,
                playlistId=self.playlist,
                pageToken=pageToken
            )
        else:
            drequest = self.youtube.playlistItems().list(
                part="snippet",
                maxResults=100,
                playlistId=self.playlist
            )
        res_dict_base = drequest.execute()
        res_dict = res_dict_base['items']
        print(len(res_dict))
        self.response = res_dict
        if 'nextPageToken' in res_dict_base:
            print(
                "Recursion activated.........................................................")
            b = self.request(res_dict_base['nextPageToken'])
            new_dict = res_dict + b
            return new_dict

        return res_dict
        # print(type(response))
        # print(response)

        # ['items'][0]['snippet']['thumbnails']['high']['url']
        # print(i['snippet']['thumbnails']['high']['url'])

    def save_json(self, dicto):
        with open(self.playlist + '_data.json', 'w') as fp:
            json.dump(dicto, fp, indent=4)

    def load_json(self):
        with open(self.playlist + '_data.json') as json_file:
            return json.load(json_file)

    def print_titles(self, json):
        print("printing titles: " + '%' * 75)
        print(len(json['items']))
        titles = []
        for i in json['items']:
            title = i['snippet']['title']
            titles.append(title)
        return titles

    def get_pic_urls(self, json):
        print("Getting urls: " + '%' * 75)
        print(len(json))
        urls = []
        for i in json:
            print(i)
            try:
                uri = i['snippet']['thumbnails']['medium']['url']
                urls.append(uri)
            except:
                pass
        return urls

    def save_pics(self, urls):
        print("saving pics: " + '%' * 75)
        print(len(urls))
        for index, url in enumerate(urls):
            path = self.PIC_FOLDER + str(index) + '.jpg'
            print("Saving:  %s/%s" % (index, len(urls)))
            urllib.request.urlretrieve(url, path)

    def create_collage(self):
        side_size = 4000
        new = Image.new("RGBA", (side_size, side_size))
        files = next(walk(self.PIC_FOLDER), (None, None, []))[2]
        files.sort(key=self.natural_keys)
        cwd = os.getcwd().replace('\\', '/')
        img_rows = math.ceil(math.sqrt(len(files)))
        resize = math.floor(side_size / img_rows)
        x1 = 0
        y1 = 0
        for x in range(img_rows):
            y1 = 0
            for y in range(img_rows):
                print('%s , %s' % (x1, y1))
                try:
                    full_path = cwd + '/' + self.PIC_FOLDER + '/' + \
                                files[img_rows * x1 + y1]
                except IndexError:
                    print("ERRROOORRR restarting indices")
                    x1 = 0
                    y1 = 0
                    full_path = cwd + '/' + self.PIC_FOLDER + '/' + \
                                files[int(img_rows * x1) + int(y1)]
                print("Loading file %s" % full_path)
                img = Image.open(full_path).resize((resize, resize))
                new.paste(img, (y * resize, x * resize))
                y1 += 1
            x1 += 1

        new.save(self.PIC_FOLDER[:-5] + '_final.png')
        new.show()

    def atoi(self, text):
        return int(text) if text.isdigit() else text

    def natural_keys(self, text):
        return [self.atoi(c) for c in re.split(r'(\d+)', text)]


if __name__ == "__main__":
    # PLKenR505wS68_6hu1AANgcNre4AojF7zb
    # PLKenR505wS69Da42X7--Jhhxihwg2WWcC
    # PLKenR505wS6-ecJ8rA85hBYcepewqYSSG
    # PL-1mdD8s_jl8FnStZNQresghDwwXYEWBu
    a = yoto("FL9PV7T-BlWgOBTNr8Gkj_Ug")
    data = a.request()
    a.save_json(data)
    json = a.load_json()
    # a.print_titles(json)
    urls = a.get_pic_urls(json)
    a.save_pics(urls)
    a.create_collage()
